import gdb
import string

gdb.execute('file ./brute')
gdb.execute('set pagination off')
gdb.Breakpoint('*0x004009a5')

pattern = string.printable

flag = ''
for i in range(len(flag), 30):
    for ch in pattern:

        open('input.txt', 'w').write(flag + ch)
        gdb.execute('run < input.txt')

        for _ in range(i + 1):
            try:
                gdb.execute('continue')
            except gdb.error:
                pass

        msg = gdb.execute('i b', to_string=True)
        if 'hit {} times'.format(i + 2) in msg:
            flag += ch
            gdb.execute('continue')
            break

        print('-' * 80)
        print('flag:', flag + ch)
        print(msg)

print(flag)
gdb.execute('quit')