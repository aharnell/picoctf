from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii

# c= '964354128913912393938480857590969826308054462950561875638492039363373779803642185'
# keypair = {
#     n: 1584586296183412107468474423529992275940096154074798537916936609523894209759157543,
#     e: 65537,
#     }

# decryptor = PKCS1_OAEP.new(n, e)
# decrypted = decryptor.decrypt(c)
# print('Decrypted:', decrypted)

keyPair = RSA.generate(3072)
# print(dir(keyPair))
keyPair.n = 1584586296183412107468474423529992275940096154074798537916936609523894209759157543
keyPair.e = 65537
pubKey = keyPair.publickey()
print(f"Public key:  (n={hex(pubKey.n)}, e={hex(pubKey.e)})")
pubKeyPEM = pubKey.exportKey()
print(pubKeyPEM.decode('ascii'))

print(f"Private key: (n={hex(pubKey.n)}, d={hex(keyPair.d)})")
privKeyPEM = keyPair.exportKey()
print(privKeyPEM.decode('ascii'))

msg = b'A message for encryption'
encryptor = PKCS1_OAEP.new(pubKey)
encrypted = encryptor.encrypt(msg)
print(type(encrypted))
print("Encrypted:", binascii.hexlify(encrypted))

# decryptor = PKCS1_OAEP.new(keyPair)
# decrypted = decryptor.decrypt(encrypted)
# print('Decrypted:', decrypted)